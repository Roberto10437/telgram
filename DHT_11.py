import Adafruit_DHT
import RPi.GPIO as GPIO
import time
import urllib.request
import requests
import multiprocessing
sensor = Adafruit_DHT.DHT11
led=37
led1=35
led2=15
GPIO.setmode(GPIO.BOARD)
GPIO.setup(led,GPIO.OUT)
GPIO.setup(led1,GPIO.OUT)
GPIO.setwarnings(False)
temMax=25
tempMin=10
gpio = 24
count=0
flag=True
leed=True
def Principal():
    global count
    global flag
    global leed
    while True:
        humidity, temperature = Adafruit_DHT.read_retry(sensor, gpio)

        # Reading the DHT11 is very sensitive to timings and occasionally
        # the Pi might fail to get a valid reading. So check if readings are valid.

        if humidity is not None and temperature is not None:
            print('Temp={0:0.1f} °C  Humidity={1:0.1f} %'.format(temperature, humidity))
            newvalue=temperature
            count+=1

        else:
            print('Failed to get reading. Try again!')
        if temMax<temperature:
            for x in range(0,10):
                GPIO.output(led,True)
                time.sleep(0.1)
                GPIO.output(led,False)
                time.sleep(0.1)
        if tempMin>temperature:
            for y in range(0,10):
                GPIO.output(led1,True)
                time.sleep(0.5)
                GPIO.output(led1,False)
                time.sleep(0.5)
        if flag==True:
            lastvalue=temperature
            flag=False
        if count==2:
            resultado=lastvalue-newvalue
            print(resultado)
            count=0
            lastvalue=temperature
            leed=False
        if count==1 and leed==False:
            resultado=abs(lastvalue-newvalue)
            print(resultado)
            if resultado > 5:
                print("error demasiada variación positiva")
            else:
                url=urllib.request.urlopen("https://api.thingspeak.com/update?api_key=6JEZLV2T8IXDZAQK&field1={}&field2={}".format(temperature,humidity))
                lastvalue=temperature
            count=0
        time.sleep(60)
def Porton():
    while True:
        estado=requests.get("http://blynk-cloud.com/1c2ecdeef3ef48509484191155904858/get/v0")
        val=estado.text
        val1=val[2]
        if val1==str(1):
            value=0
            time.sleep(2)
            requests.get("http://blynk-cloud.com/1c2ecdeef3ef48509484191155904858/update/v0?value={}".format(value))

if __name__=='__main__':
    t1=multiprocessing.Process(target=Principal)
    t2=multiprocessing.Process(target=Porton)
    t1.start()
    t2.start()
