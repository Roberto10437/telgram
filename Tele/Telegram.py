import RPi.GPIO as GPIO
import time
import logging
import requests

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# Set sensor type : Options are DHT11,DHT22 or AM2302

led2=15
GPIO.setmode(GPIO.BOARD)
GPIO.setup(led2,GPIO.OUT)
GPIO.setwarnings(False)


# Set GPIO sensor is connected to


# Use read_retry method. This will retry up to 15 times to
# get a sensor reading (waiting 2 seconds between each retry).

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')
def temperatura(update,context):
    temp1=requests.get("http://45.55.96.146/1c2ecdeef3ef48509484191155904858/get/v1")
    temp2=temp1.text
    temp3=temp2[2:6]
    update.message.reply_text("la temperatura afuera es de : "+str(temp3)+"°C")
def Dimmer(update,context):
    update.message.reply_text("Comenzo el modo dimmer")
    pwmled = GPIO.PWM(led2, 250)
    pwmled.start(0)
    for i in range(0,100+1): #range(start,stop,step)
        pwmled.ChangeDutyCycle(i)
        time.sleep(0.1)
    for i in range(100,-1,-1):
        pwmled.ChangeDutyCycle(i)
        time.sleep(0.1)


def Porton(update,context):
    value=1
    requests.get("http://blynk-cloud.com/1c2ecdeef3ef48509484191155904858/update/v0?value={}".format(value))
    if value==1:
        time.sleep(2)
        value=0
        requests.get("http://blynk-cloud.com/1c2ecdeef3ef48509484191155904858/update/v0?value={}".format(value))
        update.message.reply_text("El portón se esta abriendo o cerrando")


def help_command(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('las opciones son /temp,/porton,/dimmer,/start')


def echo(update, context):
    if (update.message.text.upper().find("PUTO AMO")>0):
        update.message.reply_text("El puto amo es tu papá Roberto Gonzalez Navarro")
    else:
        update.message.reply_text(update.message.text)


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("845781111:AAFhikbASQOGbPi2nhJzGRgNyLNqc9KDuXo", use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help_command))
    dp.add_handler(CommandHandler("temp", temperatura))
    dp.add_handler(CommandHandler("porton",Porton))
    dp.add_handler(CommandHandler("dimmer",Dimmer))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # Start the Bot
    updater.start_polling()
    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__=='__main__':
    main()